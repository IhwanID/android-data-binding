package id.ihwan.androiddatabinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.api.load

@BindingAdapter("android:loadImage")
fun loadImage(imageView: ImageView, url: String){
    imageView.load(url)
}